#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Entry',
    'name_de_DE': 'Buchhaltung Brutto Buchungsstapel',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Accounting Module with Batch Entry
    - Provides the possibility to encode batches move lines made of
      single line brut (including VAT) entries
''',
    'description_de_DE': '''Buchhaltungs Modul für Bruttobuchungen
    - Ermöglicht die Erfassung von einzeiligen Bruttobuchungen im Stapel
''',
    'depends': [
        'account_move_external_reference',
        'company',
        'currency',
        'party',
        ],
    'xml': [
        'batch.xml',
        'journal.xml',
        ],
    'translation': [
        'locale/de_DE.po'
    ],
}
