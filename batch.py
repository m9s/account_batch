#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch"
from decimal import Decimal
from time import *
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Get, Equal, Eval, Not, In, Bool, Or, And, If, \
    PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.model.browse import BrowseRecord, BrowseRecordNull


_BATCH_STATES = {
    'readonly': Equal(Eval('state'), 'closed'),
    }
BATCH_LINE_STATES = {
    'readonly': Or(Not(Bool(Eval('journal'))),(Equal(Eval('state'), 'posted')))
    }
BATCH_LINE_DEPENDS = ['journal', 'state']
_ZERO = Decimal("0.0")


class Batch(ModelSQL, ModelView):
    'Account Batch'
    _name = 'account.batch'
    _description = __doc__

    name = fields.Char('Name', required=True, states=_BATCH_STATES,
        depends=['state'])
    journal = fields.Many2One('account.batch.journal', 'Journal',
            required=True, states={
                'readonly': Or(
                    Bool(Eval('lines')),
                    Equal(Eval('state'), 'closed')
                    ),
            }, on_change=['journal', 'journal_type', 'lines'],
            select=1, depends=['journal_type', 'lines', 'state'])
    lines = fields.One2Many('account.batch.line', 'batch',
            'Batch Lines', add_remove=[
                And(
                    Or(
                        Not(Bool(Eval('batch'))),
                        Equal(Eval('active_id'), Eval('batch'))
                    ),
                    Bool(('journal', '=', Eval('journal')))
                    )],
            context={
                'journal': Eval('journal'),
                'batch': Eval('active_id')
                }, states=_BATCH_STATES, depends=['state'])
    move_lines = fields.Function(fields.One2Many('account.move.line',
            None, 'Move Lines'), 'get_move_lines')
    currency_digits = fields.Function(fields.Integer('Currency Digits',
            on_change_with=['journal']), 'get_currency_digits')
    journal_type = fields.Function(fields.Selection('selection_journal_types',
            'Journal'), 'get_journal_types')
    state = fields.Selection([
        ('open', 'Open'),
        ('closed', 'Closed'),
        ], 'State', readonly=True)

    def __init__(self):
        super(Batch, self).__init__()
        self._error_messages.update({
            'missing_journal_accounts': 'Missing accounts on Batch Journal.\n'
                    'Please configure the journal before proceeding.',
            'unposted_lines': 'There are unposted batch lines for one of '
                    'the accounts used in the journal of this batch.'
                    '\n\nYou must proceed these batch lines '
                    'before being able to create further transactions.',
            })
        self._rpc.update({'selection_journal_types': False})

    def default_state(self):
        return 'open'

    def on_change_journal(self, value):
        batch_line_obj = Pool().get('account.batch.line')
        batch_journal_obj = Pool().get('account.batch.journal')
        value = value.copy()
        res = {}
        res['journal'] = value.get('journal', False)
        if res['journal']:
            batch_journal = batch_journal_obj.browse(res['journal'])
            res['journal_type'] = batch_journal.account_journal.type
            account_journal = batch_journal.account_journal
            # For correct start balance check of bank batches there may be no
            # unposted batch lines for same accounts
            if account_journal.type in ['bank', 'cash']:
                account_ids = [account_journal.credit_account.id,
                        account_journal.debit_account.id]
                batch_lines = \
                        batch_line_obj.check_unposted_batch_lines_for_account(
                                account_ids)
                if batch_lines:
                    self.raise_user_error('unposted_lines')

            # Check for configured accounts on journal
            if (not account_journal.type == 'general'
                    and not (
                        account_journal.credit_account.id
                        and account_journal.debit_account.id)):
                self.raise_user_error('missing_journal_accounts')
        return res

    def on_change_with_currency_digits(self, vals):
        journal_obj = Pool().get('account.batch.journal')

        if vals.get('journal'):
            journal = journal_obj.browse(vals['journal'])
            return journal.currency.digits
        return 2

    def get_current_start_balance(self, ids, name):
        res = {}
        for batch in self.browse(ids):
            res[batch.id] = _ZERO
            account_ids = set([batch.journal.account_journal.debit_account.id,
                    batch.journal.account_journal.credit_account.id])
            for account_id in account_ids:
                diff = self._get_balance_difference_moves(batch)
                res[batch.id] += self._calc_start_balance(account_id) - diff
        return res

    def _get_balance_difference_moves(self, batch):
        res = Decimal('0.0')
        for line in batch.lines:
            if line.amount and line.move:
                if line.is_cancelation_move:
                    res -= line.amount
                else:
                    res += line.amount
        return res

    def _calc_start_balance(self, account_id=None):
        account_obj = Pool().get('account.account')
        if not account_id:
            return _ZERO

        if isinstance(account_id, (list)):
            account_id = account_id[0]

        with Transaction().set_context(no_rounding=True):
            account = account_obj.browse(account_id)
        return account.balance

    def get_currency_digits(self, ids, name):
        res = {}
        for batch in self.browse(ids):
            res[batch.id] = batch.journal.currency.digits

        return res

    def get_journal_types(self, ids, name):
        res = {}
        for batch in self.browse(ids):
            res[batch.id] = batch.journal.account_journal.type

        return res

    def selection_journal_types(self):
        account_journal_type_obj = Pool().get('account.journal.type')
        account_journal_type_ids = account_journal_type_obj.search([])
        account_journal_types = account_journal_type_obj.browse(
                account_journal_type_ids)
        return [(x.code, x.name) for x in account_journal_types]

    def get_move_lines(self, ids, name):
        '''
        Return the move lines that have been generated by the batchs.
        '''
        res = {}
        for batch in self.browse(ids):
            res[batch.id] = []
            for line in batch.lines:
                if not line.move:
                    continue

                for move_line in line.move.lines:
                    res[batch.id].append(move_line.id)
        return res

    def get_rec_name(self, ids, name):
        if not ids:
            return {}

        res = {}
        for batch in self.browse(ids):
            name = batch.name
            if name is None:
                name = '-'

            res[batch.id] = batch.journal.name + ' ' + name
        return res

Batch()


class Line(ModelSQL, ModelView):
    'Account Batch Entry Line'
    _name = 'account.batch.line'
    _description = __doc__
    _rec_name = 'description'

    sequence = fields.Char('Seq.', readonly=True, select=2,
            help='Chronological sequence of batch line creation')
    code = fields.Char('Code', readonly=True, select=2,
            help='Chronological sequence of posted batch lines')
    posting_text = fields.Char('Posting Text', states=BATCH_LINE_STATES,
            depends=BATCH_LINE_DEPENDS, select=1)
    batch = fields.Many2One('account.batch', 'Batch', ondelete='CASCADE',
            domain=[('journal', '=', Eval('journal'))], states={
                'readonly': Or(
                    Equal(Eval('state'), 'posted'),
                    Not(Bool(Eval('journal'))),
                    Bool(Get(Eval('context', {}), 'batch', 0))
                    )
            }, depends=['state', 'journal'], select=1)
    journal = fields.Many2One('account.batch.journal', 'Batch Journal',
            required=True, states={
                'readonly': Or(
                    Equal(Eval('state'), 'posted'),
                    Bool(Eval('journal')),
                    Bool(Get(Eval('context', {}), 'journal', 0))
                    )
            }, depends=['state', 'journal'], select=1)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS, select=2)
    date = fields.Date('Date', required=True, on_change=[
                    'date', 'fiscalyear', 'account',
                    'contra_account', 'journal', 'amount',
            ], states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS, select=1)
    amount = fields.Numeric('Amount', required=True,
            digits=(16, Eval('currency_digits', 2)),
            depends=BATCH_LINE_DEPENDS+['currency_digits'],
            states=BATCH_LINE_STATES, select=2)
    account = fields.Many2One('account.account', 'Account', required=True,
            domain=[('kind', '!=', 'view')],
            states={
                'readonly':
                    Or(
                        Or(
                            In(Eval('journal_type'), ['cash', 'bank']),
                            Not(Bool(Eval('journal_type', False)))
                        ),
                        Equal(Eval('state'), 'posted')
                    )
            }, on_change=[
                'account', 'contra_account',
            ], depends=['journal_type', 'state'], select=2)
    contra_account = fields.Many2One('account.account', 'Contra Account',
            required=True, domain=[('kind', '!=', 'view')], on_change=[
            'account', 'contra_account',
            ], states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS,
            select=2)
    side_account = fields.Function(fields.Selection([
                ('', ''),
                ('debit', 'D'),
                ('credit', 'C'),
            ], ' ', on_change_with=[
                'amount', 'journal'
            ], help="'D': Account is debit side\n"
                    "'C': Account is credit side"),
            'get_function_fields')
    side_contra_account = fields.Function(fields.Selection([
                ('', ''),
                ('debit', 'D'),
                ('credit', 'C'),
            ], ' ', on_change_with=[
                'amount', 'journal'
            ], help="'D': Contra Account is debit side\n"
                    "'C': Contra Account is credit side"),
            'get_function_fields')
    party = fields.Many2One('party.party', 'Party', on_change=[
                'amount', 'party', 'journal', 'account',
                'contra_account', 'is_cancelation_move', 'date'
            ], states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS, select=2)
    is_cancelation_move = fields.Boolean('Cancelation',
            states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS)
    move = fields.Many2One('account.move', 'Account Move', readonly=True,
            select=1)
    external_reference = fields.Char('External Reference', select=1,
            states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS,
            help='This field collects references to external documents, '
            'like voucher or receipt numbers.')
    maturity_date = fields.Date('Maturity Date', states=BATCH_LINE_STATES,
            depends=BATCH_LINE_DEPENDS, help='Date to pay the amount of the '
            'batch line at least.')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_function_fields')
    journal_type = fields.Function(fields.Char('Batch Journal Type'),
            'get_function_fields')
    state = fields.Function(fields.Selection([
                ('new', 'New'),
                ('draft', 'Draft'),
                ('posted', 'Posted'),
            ], 'State', select=2), 'get_function_fields',
            searcher='search_state')

    def __init__(self):
        super(Line, self).__init__()
        self._error_messages.update({
                'same_account_and_contra': 'Account and contra account are ' \
                        'equal!\nPlease provide different accounts.',
            })
        self._order[0] = ('id', 'DESC')
        self._BATCH_LINE_FIELDS_EXCLUDE_UPDATE_MOVE = ['code',]

    def default_sequence(self):
        return ''

    def default_date(self):
        pool = Pool()
        period_obj = pool.get('account.period')
        fiscalyear_obj = pool.get('account.fiscalyear')
        date_obj = pool.get('ir.date')
        move_obj = pool.get('account.move')
        batch_journal_obj = pool.get('account.batch.journal')

        res = date_obj.today()
        if not Transaction().context.get('journal'):
            return res

        account_journal_id = batch_journal_obj.browse(
                Transaction().context['journal']).account_journal.id

        if Transaction().context.get('period_id'):
            period = period_obj.browse(Transaction().context['period_id'])
            args = [
                ('journal', '=', account_journal_id),
                ('date', '>', period.start_date),
                ('date', '<', period.end_date)
            ]
            move_ids = move_obj.search(args, limit=1, order=[('date', 'DESC')])
            if move_ids:
                res = move_obj.read(move_ids[0], ['date'])['date']
            else:
                res = period.start_date
        elif Transaction().context.get('fiscalyear_id'):
            fiscalyear = fiscalyear_obj.browse(
                    Transaction().context['fiscalyear_id'])
            args = [
                ('journal', '=', account_journal_id),
                ('date', '>', fiscalyear.start_date),
                ('date', '<', fiscalyear.end_date)
            ]
            move_ids = move_obj.search(args, limit=1, order=[('date', 'DESC')])
            if move_ids:
                res = move_obj.read(move_ids[0], ['date'])['date']
            else:
                res = fiscalyear.start_date
        return res

    def default_side_account(self):
        return ''

    def default_currency_digits(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            return company.currency.digits
        return 2

    def default_state(self):
        return 'new'

    def default_journal(self):
        return False

    def default_get(self, fields, with_rec_name=True):
        pool = Pool()
        batch_journal_obj = pool.get('account.batch.journal')
        fiscalyear_obj = pool.get('account.fiscalyear')
        date_obj = pool.get('ir.date')
        today = date_obj.today()

        values = super(Line, self).default_get(fields,
                with_rec_name=with_rec_name)

        values = values.copy()
        date = values.get('date', today)
        company_id = Transaction().context.get('company', False)
        if 'fiscalyear' in fields:
            values['fiscalyear'] = fiscalyear_obj.find(company_id, date=date,
                    exception=True)

        if Transaction().context.get('batch'):
            if 'batch' in fields:
                values['batch'] = Transaction().context['batch']

        if Transaction().context.get('journal'):
            account_journal = batch_journal_obj.browse(
                    Transaction().context['journal']).account_journal
            account, side = self._choose_account(_ZERO, account_journal)
            if 'journal' in fields:
                values['journal'] = Transaction().context['journal']

            if 'account' in fields:
                values['account'] = account

            if 'side_account' in fields:
                values['side_account'] = side

            if 'side_contra_account' in fields:
                values['side_contra_account'] = self._opposite(side)

            if 'journal_type' in fields:
                values['journal_type'] = account_journal.type

        return values

    def _opposite(self, side):
        opposite = ''
        if side == 'debit':
            opposite='credit'
        elif side == 'credit':
            opposite = 'debit'

        return opposite

    def on_change_with_side_contra_account(self, vals):
        return self._opposite(self.on_change_with_side_account(vals))

    def on_change_with_side_account(self, vals):
        batch_journal_obj = Pool().get('account.batch.journal')
        amount = vals.get('amount', _ZERO)
        batch_journal_id = vals.get('journal' or False)
        account_journal = batch_journal_obj.browse(
                batch_journal_id).account_journal
        side =''
        if amount and account_journal:
            _, side = self._choose_account(amount, account_journal)

        return side

    def _choose_account(self, amount, account_journal):
        if amount >= _ZERO:
            account = account_journal.credit_account.id
            side = 'credit'
        else:
            account = account_journal.debit_account.id
            side = 'debit'

        if account_journal.type in ['revenue', 'cash', 'bank']:
            if amount >= _ZERO:
                account = account_journal.debit_account.id
                side = 'debit'
            else:
                account = account_journal.credit_account.id
                side = 'credit'

        return account, side

    def on_change_account(self, vals):
        account = vals.get('account', False)
        contra_account = vals.get('contra_account', False)
        res = {}
        if account == contra_account:
            res['account'] = False
        return res

    def on_change_contra_account(self, vals):
        account = vals.get('account')
        contra_account = vals.get('contra_account')
        res = {}
        if account == contra_account:
            res['contra_account'] = False
        return res

    def on_change_party(self, value):
        pool = Pool()
        party_obj = pool.get('party.party')
        batch_journal_obj = pool.get('account.batch.journal')

        res = {}
        party_id = value.get('party')
        batch_journal_id = value.get('journal')
        if party_id and batch_journal_id:
            party = party_obj.browse(party_id)
            batch_journal = batch_journal_obj.browse(batch_journal_id)
            if value.get('amount'):
                type_ = batch_journal.account_journal.type
                if type_ == 'expense':
                    res['account'] = party.account_payable.id
                elif type_ == 'revenue':
                    res['account'] = party.account_receivable.id
                elif type_ in ['cash', 'bank']:
                    if value['amount'] >= _ZERO:
                        res['contra_account'] = party.account_receivable.id
                    else:
                        res['contra_account'] = party.account_payable.id
        return res

    def on_change_date(self, value):
        pool = Pool()
        batch_journal_obj = pool.get('account.batch.journal')
        fiscalyear_obj = pool.get('account.fiscalyear')
        period_obj = pool.get('account.period')
        date_obj = pool.get('ir.date')

        today = date_obj.today()
        date = value.get('date', today)
        fiscalyear_id = value.get('fiscalyear', False)
        batch_journal_id = value.get('journal', False)
        res = {}
        company_id = Transaction().context.get('company', False)
        new_fiscalyear_id = fiscalyear_obj.find(company_id, date=date,
                exception=True)
        if new_fiscalyear_id != fiscalyear_id and batch_journal_id:
            res['fiscalyear'] = new_fiscalyear_id
            account_journal = batch_journal_obj.browse(
                    batch_journal_id).account_journal
            new_account_id, side = self._choose_account(_ZERO, account_journal)
            res['account'] = new_account_id
            res['contra_account'] = False
        if Transaction().context.get('period_id'):
            period = period_obj.browse(Transaction().context['period_id'])
            date_period = period.find(company_id, date=date, exception=False,
                    test_state=True)
            if date_period != period.id:
                res['date'] = False
        elif Transaction().context.get('fiscalyear') and \
                new_fiscalyear_id != Transaction().context['fiscalyear_id']:
            res['date'] = False
        return res

    def get_function_fields(self, ids, names):
        res = {}
        defaults = {}.fromkeys(ids, '')
        for name in names:
            if name in ['side_account', 'side_contra_account']:
                res[name] = defaults.copy()
                continue
            res[name] = {}
        for line in self.browse(ids):
            line_id = line.id
            amount = line.amount or _ZERO
            journal = line.journal
            account_journal = journal.account_journal
            _, side = self._choose_account(amount, account_journal)
            if 'side_account' in names:
                res['side_account'][line_id] = side
            if 'side_contra_account' in names:
                res['side_contra_account'][line_id] = self._opposite(side)
            if 'currency_digits' in names:
                res['currency_digits'][line_id] = journal.currency.digits
            if 'journal_type' in names:
                res['journal_type'][line_id] = account_journal.type
            if 'state' in names:
                res['state'][line_id] = line.move.state if line.move else 'new'
        return res

    def search_state(self, name, clause):
        clause2 = [('move.state', clause[1], clause[2])]
        return clause2

    def create_move_dict(self, batch_line_vals):
        '''
        Create move dictionary for the batch line

        :param batch_line_vals: a dictionary of batch line values
        :return: a dictionary which represents a move
        '''
        pool = Pool()
        period_obj = pool.get('account.period')
        journal_obj = pool.get('account.batch.journal')

        date = batch_line_vals['date']
        journal_id = batch_line_vals['journal']
        journal = journal_obj.browse(journal_id)

        period_id = period_obj.find(journal.company.id, date=date)
        account_journal_id = journal.account_journal.id
        move_lines = self._get_move_lines(batch_line_vals)
        move_dict = {
                    'period': period_id,
                    'journal': account_journal_id,
                    'date': date,
                    'external_reference': batch_line_vals.get(
                        'external_reference') or '',
                    'lines': [('create', x) for x in move_lines],
                }
        return move_dict

    def post(self, ids):
        move_obj = Pool().get('account.move')
        if isinstance(ids, (int, long)):
            ids = [ids]

        lines = self.browse(ids)
        move_obj.post([l.move.id for l in lines
                if l.move and l.state =='draft'])

    def _get_move_lines(self, batch_line_vals):
        '''
        Return the values of the move lines for the batch line

        :param batch_line_vals: a dictionary of batch line values
        :return: a list of dictionaries of move line values
        '''
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        journal_obj = pool.get('account.batch.journal')

        res = []
        amount_second_currency = Decimal('0.0')
        second_currency = False
        batch_journal = journal_obj.browse(batch_line_vals['journal'])

        amount = batch_line_vals['amount']
        if batch_journal.currency.id != \
                    batch_journal.company.currency.id:
            with Transaction().set_context(date=batch_line_vals['date']):
                amount = currency_obj.compute(
                        batch_journal.currency.id, amount,
                        batch_journal.company.currency.id)
            second_currency = batch_journal.currency.id
            amount_second_currency = abs(amount)

        _, side = self._choose_account(amount, batch_journal.account_journal)
        is_cancelation_move = batch_line_vals.get('is_cancelation_move', False)
        if side == 'credit':
            if is_cancelation_move:
                debit_account = batch_line_vals['account']
                credit_account = batch_line_vals['contra_account']
            else:
                debit_account = batch_line_vals['contra_account']
                credit_account = batch_line_vals['account']

        if side == 'debit':
            if is_cancelation_move:
                debit_account = batch_line_vals['contra_account']
                credit_account = batch_line_vals['account']
            else:
                debit_account = batch_line_vals['account']
                credit_account = batch_line_vals['contra_account']

        amount = abs(amount)
        if is_cancelation_move:
            amount *= Decimal('-1')
            amount_second_currency *= Decimal('-1')

        if debit_account == credit_account:
            self.raise_user_error('same_account_and_contra')

        res.append(self.build_credit_move_line(batch_line_vals, amount,
                credit_account, second_currency, amount_second_currency))
        res.append(self.build_debit_move_line(batch_line_vals, amount,
                debit_account, second_currency, amount_second_currency))
        return res

    def build_credit_move_line(self, vals, amount,
            credit_account_id, second_currency, amount_second_currency):
        res = {
                    'name': vals.get('posting_text') or '-',
                    'debit': _ZERO,
                    'credit': amount,
                    'account': credit_account_id,
                    'party': vals.get('party', False),
                    'second_currency': second_currency,
                    'amount_second_currency': amount_second_currency,
                    'reference': vals.get('sequence') or '',
                    'external_reference': vals.get('external_reference') or '',
                    'maturity_date': vals.get('maturity_date', False),
                }
        return res

    def build_debit_move_line(self, vals, amount,
            debit_account_id, second_currency, amount_second_currency):
        res = {
                    'name': vals.get('posting_text') or '-',
                    'debit': amount,
                    'credit': _ZERO,
                    'account': debit_account_id,
                    'party': vals.get('party', False),
                    'second_currency': second_currency,
                    'amount_second_currency': amount_second_currency,
                    'reference': vals.get('sequence') or '',
                    'external_reference': vals.get('external_reference') or '',
                    'maturity_date': vals.get('maturity_date', False),
                }
        return res

    def view_header_get(self, value, view_type='form'):
        pool = Pool()
        batch_journal_obj = pool.get('account.batch.journal')
        batch_obj = pool.get('account.batch')
        company_obj = pool.get('company.company')
        translation_obj = pool.get('ir.translation')
        period_obj = pool.get('account.period')
        fiscalyear_obj = pool.get('account.fiscalyear')

        res = super(Line, self).view_header_get(value, view_type=view_type)
        if not Transaction().context.get('journal'):
            return res

        batch_journal_ids = batch_journal_obj.search([
                    ('id', '=', Transaction().context['journal']),
                ], limit=1)
        if not batch_journal_ids:
            return res

        lang_code = 'en_US'
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            if company.lang:
                lang_code = company.lang.code

        batch_journal = batch_journal_obj.browse(batch_journal_ids[0])
        batch_ids = batch_obj.search([
                    ('id', '=', Transaction().context.get('batch')),
                ], limit=1)
        name = res
        string_source = 'Journal'
        string_translate = translation_obj._get_source(
                'account.batch.line,journal', 'field', lang_code,
                string_source) or string_source

        if Transaction().context.get('batch'):
            string_source_1 = 'Batch'
            string_translate_1 = translation_obj._get_source(
                    'account.batch.line,batch', 'field', lang_code,
                    string_source_1) or string_source_1
            batch = batch_obj.browse(batch_ids[0])
            name = name + ' | %s: %s | %s: %s' % (string_translate_1,
                    batch.name, string_translate,
                    batch_journal.rec_name)
        else:
            name += ' | %s: %s' % (string_translate, batch_journal.rec_name)

        if Transaction().context.get('period_id'):
            string_source_2 = 'Period'
            string_translate_2 = translation_obj._get_source(
                    'account.journal.period,period', 'field', lang_code,
                    string_source_2) or string_source_2
            period = period_obj.browse(Transaction().context['period_id'])
            name = name + ' | %s: %s' % (string_translate_2, period.name)
        elif Transaction().context.get('fiscalyear_id'):
            string_source_3 = 'Fiscal Year'
            string_translate_3 = translation_obj._get_source(
                    'account.batch.line,fiscalyear', 'field', lang_code,
                    string_source_3) or string_source_3
            fiscalyear = fiscalyear_obj.browse(
                    Transaction().context['fiscalyear_id'])
            name = name + ' | %s: %s' % (string_translate_3, fiscalyear.name)
        return name

    def check_unposted_batch_lines_for_account(self, account_ids=None):
        batch_line_obj = Pool().get('account.batch.line')
        if not account_ids:
            return False

        # eliminate duplicates
        account_ids = list(set(account_ids))
        batch_line_ids = batch_line_obj.search(
                ['AND', ['OR', ('account', 'in', account_ids),
                    ('contra_account', 'in', account_ids),
                    ],
                    ('state', '!=', 'posted'),
                    ('journal.account_journal.type', '!=', 'situation'),
                ], limit=1)
        if batch_line_ids:
            return True
        return False

    def set_code(self, ids):
        sequence_obj = Pool().get('ir.sequence')

        batch_lines = self.browse(ids)
        for line in batch_lines:
            if line.state != 'draft':
                continue
            self.write(line.id, {
                    'code': sequence_obj.get('account.batch.line'),
                })

    def create(self, vals):
        move_obj = Pool().get('account.move')
        sequence_obj = Pool().get('ir.sequence')

        sequence = sequence_obj.get('account.batch.line.create')
        vals['sequence'] = vals.get('sequence') or sequence

        move_data = self.create_move_dict(vals)
        move_id = move_obj.create(move_data)

        vals['move'] = move_id
        batch_line_id = super(Line, self).create(vals)
        return batch_line_id

    def write(self, ids, vals):
        res = super(Line, self).write(ids, vals)
        if self._check_update_move(vals):
            if isinstance(ids, (int, long)):
                ids = [ids]
            batch_lines = self.browse(ids)
            for batch_line in batch_lines:
                self._write_move_line(batch_line)
        return res

    def _check_update_move(self, vals):
        res = False
        fields = set(vals.keys())
        no_update_fields = set(self._BATCH_LINE_FIELDS_EXCLUDE_UPDATE_MOVE)
        if fields.difference(no_update_fields):
            res = True
        return res

    def _write_move_line(self, line):
        pool = Pool()
        move_obj = pool.get('account.move')
        move_line_obj = pool.get('account.move.line')
        reconciliation_obj = pool.get('account.move.reconciliation')

        move_id = line.move.id
        move_line_ids = [x.id for x in line.move.lines]
        # Reconciliaton has to be suspended while rewriting the move lines
        # Suspend reconciliation
        move_lines = move_line_obj.browse(move_line_ids)
        reconciliation_ids = [x.reconciliation.id for x in move_lines \
                if x.reconciliation]
        if reconciliation_ids:
            lines_to_reconcile = []
            reconciliation = reconciliation_obj.browse(reconciliation_ids[0])
            reconciliation_name = reconciliation.name
            for move_line in reconciliation.lines:
                if move_line.move.id == move_id:
                    account_to_reconcile = move_line.account
                else:
                    lines_to_reconcile.append(move_line.id)
            reconciliation_obj.delete(reconciliation_ids)

        # Delete former move lines and re-create new
        move_line_obj.delete(move_line_ids)
        batch_line_vals = self.line2vals(line)
        move_data = self.create_move_dict(batch_line_vals)
        res = move_obj.write(move_id, move_data)

        # Resume reconciliation
        if reconciliation_ids:
            new_line_to_reconcile = move_line_obj.search([
                    ('move', '=', move_id),
                    ('account', '=', account_to_reconcile)
                    ])
            lines_to_reconcile += new_line_to_reconcile
            reconciliation_obj.create({
                'name': reconciliation_name,
                'lines': [('add', x) for x in lines_to_reconcile],
                })

        return res

    def line2vals(self, line):
        res = {}
        for field in self._columns:
            value = line[field]
            if isinstance(value, BrowseRecord):
                value = value.id
            if isinstance(value, BrowseRecordNull):
                value = False
            res[field] = value
        return res

    def copy(self, ids, default=None):
        date_obj = Pool().get('ir.date')

        int_id = False
        if isinstance(ids, (int, long)):
            int_id = True
            ids = [ids]

        if default is None:
            default = {}

        default = default.copy()
        default['code'] = False
        default['move'] = False
        default.setdefault('date', date_obj.today())
        new_ids = []
        for batch_line in self.browse(ids):
            new_id = super(Line, self).copy(batch_line.id, default=default)
            new_ids.append(new_id)

        if int_id:
            return new_ids[0]

        return new_ids

    def delete(self, ids):
        move_obj = Pool().get('account.move')

        if isinstance(ids, (int, long)):
            ids = [ids]

        batch_lines = self.browse(ids)
        move_ids = [x.move.id for x in batch_lines]
        res = super(Line, self).delete(ids)
        move_obj.delete(move_ids)

        return res

    def fields_view_get(self, view_id=None, view_type='form',
                hexmd5=None):
        res = super(Line, self).fields_view_get(view_id=view_id,
                view_type=view_type, hexmd5=None)
        res = copy.copy(res)
        if res['type'] == 'tree':
            if Transaction().context.get('journal'):
                res['arch'] = res['arch'].replace('<field name="journal"/>',
                        '<field name="journal" select="0" tree_invisible="1"/>')
            if Transaction().context.get('batch'):
                res['arch'] = res['arch'].replace('<field name="batch"/>',
                        '<field name="batch" select="0" tree_invisible="1"/>')
        return res

Line()


class OpenBatchJournalAsk(ModelView):
    'Open Batch Journal Ask'
    _name = 'account.batch.open_batch_journal.ask'
    _description = __doc__
    batch_journal = fields.Many2One('account.batch.journal', 'Batch Journal',
            required=True, states={
                    'readonly': Bool(Eval('batch')),
                }, on_change_with=['batch'], depends=['batch'])
    batch = fields.Many2One('account.batch', 'Batch', domain=[
            ('journal', If(Bool(Eval('batch_journal')), '=', '!='),
                Eval('batch_journal')),
            ('state', '=', 'open'),
            ], depends=['batch_journal'])
    show_draft = fields.Boolean('Show Draft')
    show_posted = fields.Boolean('Show Posted')
    period = fields.Many2One('account.period', 'Period', domain=[
            ('fiscalyear', If(Bool(Eval('fiscalyear')), '=', '!='),
                Eval('fiscalyear'))
            ], on_change=['fiscalyear', 'period'], depends=['fiscalyear'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear',
            on_change=['fiscalyear', 'period'])
    company =  fields.Many2One('company.company', 'Company', required=True,
            domain=[('company', '=', Get(Eval('context', {}), 'company',
                False))])

    def default_show_draft(self):
        return True

    def default_show_posted(self):
        return False

    def default_company(self):
        return Transaction().context.get('company') or False

    def default_fiscalyear(self):
        return False

    def on_change_with_batch_journal(self, vals):
        batch_obj = Pool().get('account.batch')
        batch_id = vals.get('batch', False)
        if batch_id:
            batch = batch_obj.browse(batch_id)
            return batch.journal.id
        return False

    def on_change_fiscalyear(self, vals):
        res = {}
        res['period'] = None
        return res

    def on_change_period(self, vals):
        period_obj = Pool().get('account.period')
        res = {}
        period_id = vals.get('period', False)
        res['fiscalyear'] = False
        if period_id:
            period = period_obj.browse(period_id)
            res['fiscalyear'] = period.fiscalyear.id
        return res

OpenBatchJournalAsk()


class OpenBatchJournal(Wizard):
    'Open Batch Journal'
    _name = 'account.batch.open_batch_journal'
    states = {
        'init': {
            'result': {
                'type': 'choice',
                'next_state': '_next',
            },
        },
        'ask': {
            'result': {
                'type': 'form',
                'object': 'account.batch.open_batch_journal.ask',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open_batch_journal',
                'state': 'end',
            },
        },
    }

    def _next(self, data):
        if data.get('model', '') == 'account.batch.journal' \
                and data.get('id'):
            return 'open'
        return 'ask'

    def _action_open_batch_journal(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        batch_journal_id = data['form']['batch_journal']
        batch_id = data['form'].get('batch', None)
        period_id = data['form']['period']
        fiscalyear_id = data['form']['fiscalyear']
        view = 'act_batch_line_form_editable'

        act_window_id = model_data_obj.get_id('account_batch', view)
        res = act_window_obj.read(act_window_id)
        # Use name from view_header_get
        del res['name']

        domain = []
        ctx = {}
        domain.append(('journal', '=', batch_journal_id))
        ctx['journal'] = batch_journal_id
        _move_states = []
        if data['form'].get('show_draft', False):
            _move_states.append('draft')

        if data['form'].get('show_posted', False):
             _move_states.append('posted')

        domain.append(('move.state', 'in', _move_states))
        if batch_id:
            domain.append(('batch', '=', batch_id))
            ctx['batch'] = batch_id

        if period_id:
            domain.append(('move.period.id', '=', period_id))
            ctx['period_id'] = period_id

        if fiscalyear_id:
            domain.append(('fiscalyear.id', '=', fiscalyear_id))
            ctx['fiscalyear_id'] = fiscalyear_id

        res['pyson_domain'] = PYSONEncoder().encode(domain)
        res['pyson_context'] = PYSONEncoder().encode(ctx)
        return res

OpenBatchJournal()


class PostBatchLines(Wizard):
    'Post Batch Lines'
    _name = 'account.batch.post'
    states = {
        'init': {
            'actions': ['_post'],
            'result': {
                'type': 'state',
                'state': 'end',
            },
        },
    }

    def _post(self, data):
        pool = Pool()
        batch_line_obj = pool.get('account.batch.line')
        batch_obj = pool.get('account.batch')
        model = data['model']

        if model == 'account.batch':
            # Called from batch: extract batch.lines ids
            batch_line_ids = batch_obj.search_read([
                    ('id', 'in', data['ids'] ),
                ], limit=1, fields_names=['lines'])['lines']
        else:
            batch_line_ids = data['ids']

        batch_line_obj.post(batch_line_ids)
        if model == 'account.batch':
            batch_ids = data['ids']
            batch_obj.write(batch_ids, {'state': 'closed'})
        return {}

PostBatchLines()


class CancelBatchLinesAskSure(ModelView):
    'Cancel Batch Lines Ask Sure'
    _name = 'account.batch.cancel.ask.sure'
    _description = __doc__
CancelBatchLinesAskSure()


class CancelBatchLinesAskProblem(ModelView):
    'Cancel Batch Lines Ask Problem'
    _name = 'account.batch.cancel.ask.problem'
    _description = __doc__
CancelBatchLinesAskProblem()


class CancelBatchLines(Wizard):
    'Cancel Batch Lines'
    _name = 'account.batch.cancel'
    _description = __doc__
    states = {
        'init': {
            'result':{
                'type': 'choice',
                'next_state': '_check_cancelation',
            },
        },
        'ask_sure': {
            'result': {
                'type': 'form',
                'object': 'account.batch.cancel.ask.sure',
                'state': [
                    ('end', 'No', 'tryton-cancel'),
                    ('cancelation', 'Yes', 'tryton-ok', True),
                ]
            },
        },
        'ask_problem': {
            'result': {
                'type': 'form',
                'object': 'account.batch.cancel.ask.problem',
                'state': [
                    ('end', 'No', 'tryton-cancel'),
                    ('cancelation', 'Yes', 'tryton-ok', True),
                ]
            },
        },
        'cancelation': {
            'actions': ['_cancel_post'],
            'result': {
                'type': 'state',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(CancelBatchLines, self).__init__()
        self._error_messages.update({'no_period_fiscalyear': 'No open period '
            'found for fiscalyear %s.'})

    def _check_cancelation(self, data):
        batch_line_obj = Pool().get('account.batch.line')
        for batch_line in batch_line_obj.browse(data['ids']):
            if batch_line.state != 'posted':
                return 'ask_problem'
        return 'ask_sure'

    def _process_cancelation_values(self, batch_line_id):
        pool = Pool()
        batch_line_obj = pool.get('account.batch.line')
        company_obj = pool.get('company.company')
        translation_obj = pool.get('ir.translation')
        period_obj = pool.get('account.period')

        batch_line = batch_line_obj.browse(batch_line_id)

        cancelation = 'Cancelation'
        lang_code = 'en_US'
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            if company.lang:
                lang_code = company.lang.code
        cancelation = translation_obj._get_source(
                'account.batch.line,is_cancelation_move', 'field',
                lang_code, cancelation) or cancelation
        posting_text = ''
        if batch_line.posting_text:
            posting_text = ' (' + batch_line.posting_text + ')'

        posting_date = batch_line.date
        clause = [
            ('start_date', '<=', posting_date),
            ('end_date', '>=', posting_date),
            ('fiscalyear.company', '=', company.id),
            ('fiscalyear', '=', batch_line.fiscalyear),
            ('type', '=', 'standard'),
            ('state', '!=', 'close'),
            ]
        period_ids = period_obj.search(clause, order=[('start_date', 'ASC')],
            limit=1)
        if not period_ids:
            clause = ['OR', [
                    ('start_date', '<=', posting_date),
                    ('end_date', '>=', posting_date),
                    ('fiscalyear.company', '=', company.id),
                    ('fiscalyear', '=', batch_line.fiscalyear),
                    ('state', '!=', 'close'),
                ], [
                    ('start_date', '>', posting_date),
                    ('fiscalyear.company', '=', company.id),
                    ('fiscalyear', '=', batch_line.fiscalyear),
                    ('state', '!=', 'close'),
                ]]
            period_ids = period_obj.search(clause, order=[
                ('start_date', 'ASC')], limit=1)
            if not period_ids:
                self.raise_user_error('no_period_fiscalyear', error_args=(
                    batch_line.fiscalyear.name,))
            period = period_obj.browse(period_ids[0])
            if posting_date < period.start_date:
                posting_date = period.start_date

        cancelation_values = {
                'batch': batch_line.batch.id,
                'contra_account': batch_line.contra_account.id,
                'is_cancelation_move': not bool(batch_line.is_cancelation_move),
                'journal': batch_line.journal.id,
                'date': posting_date,
                'account': batch_line.account.id,
                'external_reference': batch_line.external_reference,
                'posting_text': '%s: %s%s' %(cancelation, batch_line.code,
                        posting_text),
                'amount': batch_line.amount * -1,
                'party': batch_line.party.id or None,
                'fiscalyear': batch_line.fiscalyear.id,
        }

        return cancelation_values

    def _cancel_post(self, data):
        batch_line_obj = Pool().get('account.batch.line')

        for batch_line in batch_line_obj.browse(data['ids']):
            if batch_line.state != 'posted':
                continue
            cancelation_values = self._process_cancelation_values(
                    batch_line.id)
            cancel_batch_line_id = batch_line_obj.create(cancelation_values)
            batch_line_obj.post(cancel_batch_line_id)
        return {}

CancelBatchLines()


class ShowMoveLines(Wizard):
    'Show Move Lines'
    _name = 'account.batch.show_move_lines'
    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_show_move_lines',
                'state': 'end',
            },
        },
    }

    def _show_move_lines(self, data):
        pool = Pool()
        batch_line_obj = pool.get('account.batch.line')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        batch_lines = batch_line_obj.browse(data['ids'])
        move_ids = [x.move.id for x in batch_lines]
        act_window_id = model_data_obj.get_id('account',
                'act_move_line_form')
        res = act_window_obj.read(act_window_id)
        # Use name from view_header_get
        del res['name']
        domain = [('move.id', 'in', move_ids)]
        res['domain'] = str(domain)
        res['pyson_domain'] = PYSONEncoder().encode(domain)
        return res

ShowMoveLines()
