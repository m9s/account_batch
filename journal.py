#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch Entry Journal"
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool


class Journal(ModelSQL, ModelView):
    'Batch Journal'
    _name = 'account.batch.journal'
    _description = __doc__

    name = fields.Char('Name', required=True)
    account_journal = fields.Many2One('account.journal', 'Account Journal',
            required=True)
    currency = fields.Many2One('currency.currency', 'Currency', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    def default_currency(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            return company.currency.id
        return False

    def default_company(self):
        return Transaction().context.get('company') or False

Journal()
