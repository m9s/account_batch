# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    def post(self, ids):
        batch_line_obj = Pool().get('account.batch.line')

        batch_line_ids = batch_line_obj.search([
                ('move', 'in', ids),
            ])
        if batch_line_ids:
            batch_line_obj.set_code(batch_line_ids)
        return super(Move, self).post(ids)
Move()
